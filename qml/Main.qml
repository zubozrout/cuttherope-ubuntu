import QtQuick 2.9
import QtQuick.Window 2.2
import QtSystemInfo 5.0
import QtWebEngine 1.9
import Morph.Web 0.1

Window {
    id: window
    width: 800
    height: 600
    // visibility: Window.FullScreen
    title: "Cut the Rope"
    
    property string homePage: "../game/CutTheRope/index.html"
    
    Component.onCompleted: {
        window.showFullScreen();
    }
    
    ScreenSaver {
        id: screenSaver
        screenSaverEnabled: !(Qt.application.state === Qt.ApplicationActive)
    }
    
    Item {
        id: root
        anchors.fill: parent
        
        WebEngineView {
            id: webview
            x: 0
            y: 0
            width: 1024
            height: 768
            transformOrigin: Item.TopLeft // Zoom-in from top-left corner
            
            url: homePage
            settings.webGLEnabled: true
            
            profile: WebEngineProfile{
                httpUserAgent: "" // Just unset the userAgent so the game loads, seems not to like the default (mobile)
                offTheRecord: false
                persistentStoragePath: "/home/phablet/.config/cuttherope.zubozrout"
                
            }
            
            onUrlChanged: {
                var _url = url.toString();
                var fileUrl = /file:/;
                console.log("url is " + _url);

                if(!fileUrl.test(_url)) {
                    console.log("launching externally");
                    Qt.openUrlExternally(_url);
                    url = homePage;
                }
            }
            
            transform: Scale {
                xScale: root.width / webview.width
                yScale: root.height / webview.height
            }
            
            Timer {
                interval: 4000
                running: true
                repeat: false
                onTriggered: {
                    webview.runJavaScript("document.querySelector('#gameArea').offsetWidth", function(width) {
                        webview.width = width;
                    });
                    
                    webview.runJavaScript("document.querySelector('#gameArea').offsetHeight", function(height) {
                        webview.height = height;
                    });
                }
            }
        }
    }
    
    Splash {}
}
