import QtQuick 2.9
import Lomiri.Components 1.3
import QtGraphicalEffects 1.0

Item {
    id: splash
    anchors.fill: parent
    
    Timer {
        id: splashscreen
        interval: 5000
        running: true
        repeat: false
        triggeredOnStart: false
        
        onTriggered: {
            splashscreen_art.visible = false;
        }
    }
    
    Image {
        id: splashscreen_art
        source: "../assets/zeptolab.png"
        anchors.fill: parent
    }
    
    Item {
        id: progressContainer
        x: splashscreen_art.width / 2 - width / 2 - 7
        y: splashscreen_art.height * (529/577)
        width: splashscreen_art.height * (260/577)
        height: splashscreen_art.width * (19/1024)
        
        Image {
            id: splashscreen_loader
            anchors.fill: parent
            source: "../assets/loaderbar_full.png"
            visible: false
        }
        
        Rectangle {
            id: maskProgress
            anchors.fill: parent
            color: "transparent"
            visible: false
            
            onWidthChanged: {
                maskProgressNumberAnimation.to = width;
                animationProgress.restart();
            }
            
            Rectangle {
                id: maskProgressRectangle
                color: "red"
                width: 0
                height: parent.height
                
                SequentialAnimation on width {
                    id: animationProgress
                    
                    NumberAnimation {
                        id: maskProgressNumberAnimation
                        to: maskProgress.width
                        duration: splashscreen.interval
                    }
                }
            }
        }
        
        OpacityMask {
            id: progressEffect
            visible: splashscreen_art.visible
            source: splashscreen_loader
            maskSource: maskProgress
            anchors.fill: splashscreen_loader 
        }
    }
}
